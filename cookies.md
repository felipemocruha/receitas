# Cookies

## Ingredientes

- 140g de farinha de trigo
- 3g de sal
- 115g de manteiga
- 185g de pasta de amendoim
- 60g de açucar de confeiteiro
- 80g de açúcar mascavo
- extrato de baunilha
- 1 ovo
- 1 barra de chocolate

## Preparo

Misturar os ingredientes líquidos, depois o açúcar, depois o ovo e a baunilha, a farinha com o sal e por último 2/3 do chocolate. colocar o resto do chocolate nas bolas moldadas.

Deixar de 13-15 minutos no forno a 180C
