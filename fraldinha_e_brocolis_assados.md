# Fraldinha e Brócolis Assados

## Ingredients

- 1 peça de fraldinha
- brócolis
- 6 dentes de alho
- sal + pimenta + alho em pó
- folhas de tomilho
- azeite

## Preparo

Colocar o brócolis no fundo de uma assadeira mais alta junto com azeite, colocar a fraldinha temperada sobre o rack para soltar a gordura sobre o brócolis. Assar por 23 minutos a 250C.

**Massa**:

- 300g de cream cheese
- 200g de creme de leite batido 
- 60g de açúcar de confeiteiro
- 1 colher de chá de extrato baunilha
- Um pouco de suco de limão
- 1 pitada de sal

**Base**:

- Bolacha dixavada
- Manteiga
