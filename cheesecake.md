# Cheesecake

## Ingredientes

**Massa**:

- 300g de cream cheese
- 200g de creme de leite batido 
- 60g de açúcar de confeiteiro
- 1 colher de chá de extrato baunilha
- Um pouco de suco de limão
- 1 pitada de sal

**Base**:

- Bolacha dixavada
- Manteiga
